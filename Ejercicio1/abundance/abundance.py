# -*- coding: utf-8 -*-

import math

# All perfect numbers, http://web.mit.edu/adorai/www/perfectnumbers.html, we selected the first 12 numbers, if require, we could add all perfect number to list.
perfect_numbers=[6L,28L, 496L, 8128L, 33550336L, 8589869056L,137438691328L,2305843008139952128L,2658455991569831744654692615953842176L,
191561942608236107294793378084303638130997321548169216L,13164036458569648337239753460458722910223472318386943117783728128L,
14474011154664524427946373126085988481573677491474835889066354349131199152128L,
23562723457267347065789548996709904988477547858392600710143027597506337283178622239730365539602600561360255566462503270175052892578043215543382498428777152427010394496918664028644534128033831439790236838624033171435922356643219703101720713163527487298747400647801939587165936401087419375649057918549492160555646976L]

def is_perfect(number):
    if number in perfect_numbers:
        return True
    return False


def is_abundant(number):
    if is_perfect(number):
        return False

    #The smallest odd abundant number is 945.
    if number % 2 != 0 and number < 945:
        return False
    if number == 945: 
        return True
    #The smallest abundant number not divisible by 2 or by 3 is 5391411025
    if number == 5391411025: 
        return True
    if number < 5391411025:
        if number % 2 != 0 and number % 3 != 0:
            return False

    #loop checking
    divisor_limitation = math.sqrt(number)
    sum = 1
    divisor = 2

    if number % divisor_limitation == 0:
        sum += divisor_limitation
    
    while divisor < divisor_limitation:
        
        if number % divisor == 0:
            sum += divisor
            sum += number/divisor
            
        if sum > number:
            return True
        divisor=divisor+1
        
    return False

def test_abundant(numbers):
    for number in numbers:
        if is_perfect(number):
            print  "%d is perfect" %number
        elif  is_abundant(number):
            print "%d is abundant" %number
        else :
            print "%d is deficient" %number
             



