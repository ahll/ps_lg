# -*- coding: utf-8 -*-

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../abundance/')))

import abundance

import unittest


class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_perfect_number(self):
        assert abundance.is_perfect(5)==False  
        assert abundance.is_perfect(6)==True    
        assert abundance.is_perfect(6L)==True
        assert abundance.is_perfect(7L)==False
        assert abundance.is_perfect(191561942608236107294793378084303638130997321548169216L)==True
        assert abundance.is_perfect(191561942608236107294793378084303638130997321548169213L)==False
    def test_abundant_number(self):
        assert abundance.is_abundant(5)==False  
        assert abundance.is_abundant(12)==True
        assert abundance.is_abundant(18)==True
        assert abundance.is_abundant(36)==True
        assert abundance.is_abundant(21)==False
        assert abundance.is_abundant(6L)==False
        assert abundance.is_abundant(7L)==False
        assert abundance.is_abundant(191561942608236107294793378084303638130997321548169216L)==False
        assert abundance.is_abundant(19156194260823L)==False
    def test_abundant_list(self):
        abundance.test_abundant([5,6,7,8,12,21,191561942608236107294793378084303638130997321548169216L])
        abundance.test_abundant([12,18,20,24,30,36,40,42,48,54,56,60,66,70,72,78,
 80,84,88,90,96,100,102,104,108,112,114,120,126,
 132,138,140,144,150,156,160,162,168,174,176,180,
 186,192,196,198,200,204,208,210,216,220,222,224,
 228,234,240,246,252,258,260,264,270])

        abundance.test_abundant([1,2,3,4,5,7,8,9,10,11,13,14,15,16,17,19,21,22,23,
 25,26,27,29,31,32,33,34,35,37,38,39,41,43,44,45,
 46,47,49,50,51,52,53,55,57,58,59,61,62,63,64,65,
 67,68,69,71,73,74,75,76,77,79,81,82,83,85,86])

if __name__ == '__main__':
    unittest.main()
