# Test code for Logtrues#



## Test 1##

### Python project, structure reference http://docs.python-guide.org/en/latest/writing/structure/ ###

### Code in Ejercicio1\abundance ###
### Test code in Ejercicio1\tests ###
### Deficient, Abundant, Perfect Number detection ###
### Time Complexity: O( sqrt( n )) ###
### Auxiliary Space: O( 1 ) ###
### Optimitions: ###
####      Static perfect number table from http://web.mit.edu/adorai/www/perfectnumbers.html 
####  
####  	  The smallest odd abundant number is 945.
		if number % 2 != 0 and number < 945:
			return False
		if number == 945: 
			return True
####  
####        The smallest abundant number not divisible by 2 or by 3 is 5391411025
	    if number == 5391411025: 
        	return True
		if number < 5391411025:
			if number % 2 != 0 and number % 3 != 0:
				return False
	  Loop from 2 to sqrt(n)
####  

## Test 2 ##
Reactjs + d3 + c3js

 
app.js for configuration

src/ codes

src/main/data/ copy of test data 

target/ build directory 


###for execution:###
 npm install 

 gulp clean  

 gulp build  

 gulp hot    

 See result in url localhost:4000/chart.html


Lastly, I left a static build html in the target/web. If you have not node and modern Javascript
configuration done, you could directly open the chart.html in this dir.




