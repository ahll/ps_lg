
var groupBy = function (xs, func) {
    return xs.reduce(function (rv, x) {
        (rv[func(x)] = rv[func(x)] || []).push(x);
        return rv;
    }, {});
};

var axis = {
    x: {
        type: 'timeseries',
        tick: {
            format: '%Y-%m-%d',
            rotate: 90
        }
    },

    y: {
        show: true,
        label: {
            text: 'High',
            position: 'outer-middle'
        },
        tick: {
            format: D3.format('.2f')
        }
    }
};

var PieChart = function (props) {

    var dataScheme = {

        columns: [],
        type: 'pie'

    };

    for (var key in props.data) {

        var dataColumns = [key];

        props.data[key].forEach(e => {
            dataColumns.push(e.value);
        });
        dataScheme.columns.push(dataColumns);
    }

    return React.createElement(C3Chart, { type: 'pie', data: dataScheme });
};

var DataChart = function (props) {

    var dataScheme = {
        xs: {},
        columns: [],
        axes: {}

    };

    for (var key in props.data) {
        dataScheme.axes[key] = 'y';
        dataScheme.xs[key] = 'date' + key;
        var dateColumns = [dataScheme.xs[key]];
        var dataColumns = [key];

        props.data[key].forEach(e => {
            dateColumns.push(e.date);
            dataColumns.push(e.value);
        });
        dataScheme.columns.push(dateColumns);
        dataScheme.columns.push(dataColumns);
    }

    return React.createElement(C3Chart, { data: dataScheme, axis: axis });
};

class ChartContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.loadDatafromServer(props)
        };
    }

    loadDatafromServer(props) {

        var dataList = [];

        //normalize data source one
        DataSourceOne.forEach(e => {
            dataList.push({
                cat: e.cat.toUpperCase(),
                date: new Date(e.d),
                value: e.value
            });
        });

        //normalize data source two
        DataSourceTwo.forEach(e => {
            dataList.push({
                cat: e.categ.toUpperCase(),
                date: new Date(e.myDate),
                value: e.val
            });
        });

        //normalize data source three
        DataSourceThree.forEach(e => {
            var stringRegDate = /\d{4}-\d{2}-\d{2}/;
            var row = e.raw.toUpperCase();
            var dateText = e.raw.match(stringRegDate);
            var position = row.indexOf("#CAT");
            if (position > 0) {
                row = row.substring(position + 1, position + 6);
            }
            dataList.push({
                cat: row,
                date: new Date(dateText),
                value: e.val
            });
        });

        var dataGroups = groupBy(dataList, function (element) {
            return element.cat;
        });

        //sum of same data
        for (var key in dataGroups) {
            var timeDataMap = {};
            console.log(dataGroups[key]);
            dataGroups[key].forEach(e => {
                if (!timeDataMap[e.date.getTime()]) {
                    timeDataMap[e.date.getTime()] = e;
                } else {
                    timeDataMap[e.date.getTime()].value += e.value;
                }
            });
            dataGroups[key] = [];

            for (var timeKey in timeDataMap) {
                dataGroups[key].push(timeDataMap[timeKey]);
            }
            console.log(dataGroups[key]);
        }

        return dataGroups;
    }

    render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'h1',
                null,
                'Data chart'
            ),
            React.createElement(DataChart, { data: this.state.data }),
            React.createElement(
                'h1',
                null,
                'pie chart'
            ),
            React.createElement(PieChart, { data: this.state.data })
        );
    }
};

window.onload = function () {

    ReactDOM.render(React.createElement(
        ChartContainer,
        null,
        ' '
    ), document.getElementById("main"));
};