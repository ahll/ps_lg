React = require('react');
ReactDOM = require('react-dom');
D3 = require("d3");
moment = require('moment');
C3Chart = require('react-c3js').default;

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

DataSourceOne =require("./data/dataOne.json");
DataSourceTwo =require("./data/dataTwo.json");
DataSourceThree =require("./data/dataThree.json");